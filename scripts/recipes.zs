craftingTable.addShaped("calculation_press", <item:appliedenergistics2:calculation_processor_press>, [
    [<item:minecraft:iron_ingot>, <item:minecraft:quartz>, <item:minecraft:iron_ingot>],
    [<item:minecraft:quartz>, <item:minecraft:iron_block>, <item:minecraft:quartz>],
    [<item:minecraft:iron_ingot>, <item:minecraft:quartz>, <item:minecraft:iron_ingot>]
]);

craftingTable.addShaped("engineering_press", <item:appliedenergistics2:engineering_processor_press>, [
    [<item:minecraft:iron_ingot>, <item:minecraft:diamond>, <item:minecraft:iron_ingot>],
    [<item:minecraft:diamond>, <item:minecraft:iron_block>, <item:minecraft:diamond>],
    [<item:minecraft:iron_ingot>, <item:minecraft:diamond>, <item:minecraft:iron_ingot>]
]);

craftingTable.addShaped("logic_press", <item:appliedenergistics2:logic_processor_press>, [
    [<item:minecraft:iron_ingot>, <item:minecraft:gold_ingot>, <item:minecraft:iron_ingot>],
    [<item:minecraft:gold_ingot>, <item:minecraft:iron_block>, <item:minecraft:gold_ingot>],
    [<item:minecraft:iron_ingot>, <item:minecraft:gold_ingot>, <item:minecraft:iron_ingot>]
]);

craftingTable.addShaped("silicon_press", <item:appliedenergistics2:silicon_press>, [
    [<item:minecraft:iron_ingot>, <item:appliedenergistics2:silicon>, <item:minecraft:iron_ingot>],
    [<item:appliedenergistics2:silicon>, <item:minecraft:iron_block>, <item:appliedenergistics2:silicon>],
    [<item:minecraft:iron_ingot>, <item:appliedenergistics2:silicon>, <item:minecraft:iron_ingot>]
]);

craftingTable.addShaped("sky_stone", <item:appliedenergistics2:sky_stone_block>, [
    [<item:minecraft:obsidian>, <item:minecraft:coal_block>, <item:minecraft:obsidian>],
    [<item:minecraft:coal_block>, <item:minecraft:stone>, <item:minecraft:coal_block>],
    [<item:minecraft:obsidian>, <item:minecraft:coal_block>, <item:minecraft:obsidian>]
]);

craftingTable.addShapeless("Codex_Arcana", <item:mana-and-artifice:guide_book>, [<item:minecraft:book>, <item:mana-and-artifice:vinteum_dust>]);

import crafttweaker.api.tag.MCTag;
import crafttweaker.api.item.MCItemDefinition;

craftingTable.addShapedMirrored("plank_conversion", <item:minecraft:oak_planks> * 9, [
    [<tag:items:minecraft:planks>, <tag:items:minecraft:planks>, <tag:items:minecraft:planks>],
    [<tag:items:minecraft:planks>, <tag:items:minecraft:planks>, <tag:items:minecraft:planks>],
    [<tag:items:minecraft:planks>, <tag:items:minecraft:planks>, <tag:items:minecraft:planks>]
]);
craftingTable.removeByName("minecraft:diorite");
craftingTable.addShapedMirrored("improved_diorite", <item:minecraft:diorite> * 8, [
    [<item:minecraft:cobblestone>, <item:astralsorcery:marble_raw>],
    [<item:astralsorcery:marble_raw>, <item:minecraft:cobblestone>]
]);

craftingTable.removeByName("iceandfire:copper_helmet");
craftingTable.removeByName("iceandfire:copper_chestplate");
craftingTable.removeByName("iceandfire:copper_leggings");
craftingTable.removeByName("iceandfire:copper_boots");
craftingTable.removeByName("iceandfire:copper_sword");
craftingTable.removeByName("iceandfire:copper_shovel");
craftingTable.removeByName("iceandfire:copper_pickaxe");
craftingTable.removeByName("iceandfire:copper_axe");
craftingTable.removeByName("iceandfire:copper_hoe");

<recipetype:appliedenergistics2:grinder>.addRecipe("grinder_aluminum", <item:immersiveengineering:dust_aluminum>, <item:immersiveengineering:ore_aluminum>, 4, [<item:immersiveengineering:dust_aluminum> % 90]);
<recipetype:appliedenergistics2:grinder>.addRecipe("grinder_lead", <item:thermal:lead_dust>, <item:thermal:lead_ore>, 6, [<item:thermal:lead_dust> % 90]);
<recipetype:appliedenergistics2:grinder>.addRecipe("grinder_tin", <item:thermal:tin_dust>, <item:thermal:tin_ore>, 5, [<item:thermal:tin_dust> % 90]);
<recipetype:appliedenergistics2:grinder>.addRecipe("grinder_copper", <item:thermal:copper_dust>, <item:thermal:copper_ore>, 4, [<item:thermal:copper_dust> % 90]);
<recipetype:appliedenergistics2:grinder>.addRecipe("grinder_nickel", <item:thermal:nickel_dust>, <item:thermal:nickel_ore>, 7, [<item:thermal:nickel_dust> % 90]);
<recipetype:appliedenergistics2:grinder>.addRecipe("grinder_silver", <item:thermal:silver_dust>, <item:thermal:silver_ore>, 6, [<item:thermal:silver_dust> % 90]);

<recipetype:immersiveengineering:crusher>.addRecipe("crusher_certus", <item:appliedenergistics2:certus_quartz_crystal>, 2000, <item:appliedenergistics2:certus_quartz_dust>);
<recipetype:immersiveengineering:crusher>.addRecipe("crusher_quartz", <item:minecraft:quartz>, 2000, <item:appliedenergistics2:nether_quartz_dust>);
<recipetype:immersiveengineering:crusher>.addRecipe("crusher_skystone", <item:appliedenergistics2:sky_stone_block>, 2000, <item:appliedenergistics2:sky_dust>);
<recipetype:immersiveengineering:crusher>.addRecipe("crusher_fluix", <item:appliedenergistics2:fluix_crystal>, 2000, <item:appliedenergistics2:fluix_dust>);
<recipetype:thermal:pulverizer>.addRecipe("pulverizer_certus", [<item:appliedenergistics2:certus_quartz_dust> % 100] , <item:appliedenergistics2:certus_quartz_crystal>, 0, 2000);
<recipetype:thermal:pulverizer>.addRecipe("pulverizer_skystone", [<item:appliedenergistics2:sky_dust> % 100] , <item:appliedenergistics2:sky_stone_block>, 0, 2000);
<recipetype:thermal:pulverizer>.addRecipe("pulverizer_fluix", [<item:appliedenergistics2:fluix_dust> % 100] , <item:appliedenergistics2:fluix_crystal>, 0, 2000);